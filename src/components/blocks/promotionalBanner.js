import React from "react";
// import Carousel from 'react-bootstrap/Carousel'
import "./blocks.css";

const Promotionalbanner = () =>{
    return(
        <div className="jumbotron jumbotron-fluid intro-section">
            <div className="container">
                <h1 className="display-4 banner-header">Generation Next Sports Club</h1>
                <p className="lead" style={{color:"lightgray"}}>Cricket Coaching Club Navi Mumbai</p>
            </div>
        </div>

        // <Carousel>
        //     <Carousel.Item interval={1000}>
        //     <img className="d-block" src="imgs/bannerImg/1.png" alt="" />
        //     </Carousel.Item>

        //     <Carousel.Item interval={500}>
        //     <img className="d-block " src="imgs/bannerImg/2.png" alt="" />
        //     </Carousel.Item>

        //     {/* <Carousel.Item>
        //     <img className="d-block" src="" alt="" />
        //     </Carousel.Item> */}
        // </Carousel>
    );

};

export default Promotionalbanner;