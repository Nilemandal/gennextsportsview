import React , { useState , useEffect } from "react"
import { Col, Container, Row } from "reactstrap";
import getRequestData from "../api/getRequest";
import { formatAMPM , weekDaysFunc} from "../helper/dateFunction";
import Enquiry from './block/enquiry';
import Info from "./block/Info";
import "./blocks.css";

const SportsSession = () =>{

    const [ batchData, setBatchData ] = useState({})
    const [ getError , setError ] = useState(true);
    // const [ showModal , setModal ] = useState(false);

    useEffect( () => {
        let getFetch = async () => {
            let res = await getRequestData('api/get-batches');
            if( res.status === 'success'){
                setBatchData(res.data);
                setError(false);
            }else{
                setError(true);
            }
        }
        getFetch(); 

    }, []);

    return(
        <React.Fragment>
            <Container>
                <h1 className="display-5 text-center">
                    Cricket Batch
                </h1>
                <hr/>
                <Row className="justify-content-md-center mt-2" style={{overflowY: "scroll" , maxHeight:'590px'}}>
                    {/* <ModalForm id="myCheck" buttonLabel="Enquiry"/> */}
                    {
                        (getError)? <span key="err-msg"> oops getting err while fetching... </span>  :
                        batchData.map( d => {
                            return (
                                <Col key={d.bid+'_test'} md="3" xs="11"  className="sports-card-style">
                                    <div className="batch-detail" id={d.bid} key={'batch_id_'+d.bid}>
                                        <h3 className="mt-1 pt-2" key={d.bid+'_batch_name'}> {d.batch_name} </h3>
                                        <span>
                                            Time : <span> {formatAMPM(d.start_time)} </span> to <span> {formatAMPM(d.end_time)} </span>
                                        </span> 
                                        <p key={d.bid+'_batch_capcity'}> Batch Capacity : {d.batch_capacity} </p> 
                                        <p key={d.bid+'_batch_fess'}> Batch Fees : {d.batch_fees} </p> 
                                        <p key={d.bid+'_batch_days'}> Days : [ { weekDaysFunc(d.weekdays) } ] </p>
                                        <Row className="mb-3">
                                            <Col>
                                                <Enquiry key={d.bid+'_batch_enquiry'} batchInfo={d} />
                                            </Col>
                                            <Col>
                                                <Info key={d.bid+'_batch_apply'} buttonLabel="Apply Now" className="btn-style enq-btn" size="sm" batch={d} />
                                            </Col>
                                        </Row>
                                    </div>
                                </Col>
                            )
                        })
                    }
                </Row>
            </Container> 
        </React.Fragment>
    );
};
export default SportsSession;