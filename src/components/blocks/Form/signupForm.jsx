import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { NotificationManager } from "react-notifications";
import postRequest from "../../api/postRequest";
export default class SignUp extends Component {
    state = {
        name:'',
        phone:'',
        email:'',
        password:'',
        otp:'',
        otpStatus:false,
        otpVerified:false,
        valid : false
    }

    formSubmit = false;

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
        if(e.target.name !== 'otp'){
            this.formSubmit = false;
            this.setState({otpStatus:false})
        }
    }

    sendOtp = async () =>{
        const url = 'api/send-otp';
        const data = { phone:this.state.phone };
        let otpRes = await postRequest(url,data);
        if( otpRes.status === 'success' ){
            this.formSubmit = true;
            NotificationManager.success("OTP sent successfully",'Sign Up', 2000);
            this.setState({otpStatus:true})
        }else if(otpRes.status === 'err'){
            this.formSubmit = false;
            NotificationManager.error(otpRes.msg,'Error',2000);
        }
    }

    submitFormAdd = async (e) => {
        e.preventDefault()
        await this.Validation()

        if(this.state.valid && !this.state.otpStatus){
            this.sendOtp();
            this.state.otpStatus = false;
        }

        if(this.state.otpStatus && this.state.otp){
            const regUrl = `api/user/registration`;
            const regData = this.state;
            let regStatus = await postRequest(regUrl,regData);
            if( regStatus.status === 'success'){
                NotificationManager.success("User Registered successfully",'Registered successfully', 2000);
                localStorage.setItem('auth',regStatus.token);
                this.props.toggle();
                window.location.reload();
            }else if(regStatus.status === 'err'){
                NotificationManager.error(regStatus.msg,'Error',2000);
            }
        }
    }

    Validation = () => {
        this.formSubmit=false;
        if (!this.state.name){
            return NotificationManager.error("Please Enter your Name", "Info", 2000);
        }
        else if (!this.state.phone){
            return NotificationManager.error("Please Enter Phone No.", "Info", 2000);
        }
        else if( !(/^\d{10}$/).test(this.state.phone) ){
            return NotificationManager.error("Please Enter 10 Digit Phone No.", "Info", 2000);
        }
        else if (!this.state.email){
            return NotificationManager.error("Please Enter Email", "Info", 2000);
        }
        else if (!this.state.password){
            return NotificationManager.error("Please Enter Password", "Info", 2000);
        }
        else if( this.state.otpStatus && !this.state.otp ){
            return NotificationManager.error("OTP is Missing",'OTP is Required', 2000);
        }
        else{
            this.setState({valid : true});
            //this.formSubmit=true;
        }
          
    }

    render() {
        return (
            <Form onSubmit={this.submitFormAdd}>
                <FormGroup>
                    <Label> Name</Label>
                    <Input type="text" name="name" className="form-control" onChange={this.onChange} placeholder="First name" />
                </FormGroup>

                <FormGroup>
                    <Label>Phone </Label>
                    <Input type="number" name="phone" className="form-control" onChange={this.onChange} placeholder="Enter Phone" />
                </FormGroup>

                <FormGroup>
                    <Label>Email address</Label>
                    <Input type="email" name="email" className="form-control" onChange={this.onChange} placeholder="Enter email" />
                </FormGroup>

                <FormGroup>
                    <Label>Password</Label>
                    <Input type="password" name="password" className="form-control" onChange={this.onChange} placeholder="Enter password" />
                </FormGroup>

                { 
                    (this.formSubmit)?
                    <FormGroup id="otp-div">
                    <Label>OTP </Label> 
                    <Input type="number" name="otp" className="form-control" onChange={this.onChange} maxLength="4" minLength="4" placeholder="Enter OTP" />
                    <p className="forgot-password text-right" style={{marginTop:'8px'}}>
                        <span style={{cursor:'pointer'}} onClick={ ()=>{ this.sendOtp() } } > Resend OTP </span>
                        {/* Already registered <a href="#">sign in?</a> */}
                    </p>
                    </FormGroup>:''
                }
                <Button type="submit" className="btn btn-primary btn-block">Sign Up</Button>
                
            </Form>
        );
    }
}