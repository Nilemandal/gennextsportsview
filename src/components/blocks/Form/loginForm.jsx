import React, { Component } from "react";
import { NotificationManager } from "react-notifications";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import postRequest from "../../api/postRequest";

export default class Login extends Component {
    state = {
        phone:'',
        password:'',
        otp:'',
        otpSend:false,
        byOtp:false,
        passwordLogin:true,
    }

    onChange = (e)=>{
        this.setState( {[e.target.name] : e.target.value});
        if(e.target.name === 'otp'){
            this.setState({otpSend:true});
        }
    }

    sendOtp = async () =>{
        if(!this.state.phone && !(/^\d{10}$/).test(this.state.phone) ){
            return NotificationManager.error("Please Check Phone No.", "Info", 2000);
        }

        this.setState(
            { 
                byOtp : true ,
                passwordLogin : false 
            }, 
            async ( ) => {
                const url = 'api/send-otp';
                const data = { phone:this.state.phone,signIn:1 };
                if(this.state.byOtp){
                    let otpRes = await postRequest(url,data);
                    if(  otpRes.status === 'success' ){
                        this.formSubmit = true;
                        NotificationManager.success("OTP sent successfully",'Sign Up', 2000);
                        this.setState({otpSend:true})
                    }else if(otpRes.status === 'err'){
                        this.formSubmit = false;
                        NotificationManager.error(otpRes.msg,'Error',2000);
                    }
                }else{
                    console.log('console at login component otp');
                }
            }
        )
    }

    submitFormAdd = async (e) => {
        e.preventDefault();
        if(!this.state.phone && !(/^\d{10}$/).test(this.state.phone)){
            return NotificationManager.error("Please Check Phone No.", "Info", 2000);
        }else if(this.state.passwordLogin && !this.state.password){
            return NotificationManager.error("Please Enter Password",'Info', 2000);
        }else if(this.state.byOtp && !this.state.otp){
            return NotificationManager.error("OTP is Missing",'OTP is Required', 2000);
        }

        const signInUrl = `api/user/login`;
        const signInData = this.state;
        let signInRes = await postRequest(signInUrl,signInData);
        if(signInRes.status === 'success' ){
            localStorage.setItem('auth',signInRes.token);
            this.props.toggle();
            window.location.reload(false);
            NotificationManager.success('Sign In Successfully','Info',2000);
        }else{
            this.props.toggle();
            return NotificationManager.error(signInRes.msg,'Info', 2000);
        }

    }


    render() {
        return (
            <Form onSubmit={this.submitFormAdd}>
                <FormGroup>
                    <Label>Phone </Label>
                    <Input type="number" name="phone" className="form-control" onChange={this.onChange} placeholder="Enter Phone" />
                </FormGroup>

                {
                    (this.state.passwordLogin)?
                    <FormGroup>
                        <Label>Password</Label>
                        <Input type="password" name="password" onChange={this.onChange}  className="form-control" placeholder="Enter password" />
                    </FormGroup>:''
                }
                

                {/* <FormGroup>
                    <div className="custom-control custom-checkbox">
                        <Input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <Label className="custom-control-label" htmlFor="customCheck1">Remember me</Label>
                    </div>
                </FormGroup> */}

                { 
                    (this.state.byOtp)?
                    <FormGroup id="otp-div">
                    <Label>OTP </Label> 
                    <Input type="number" name="otp" className="form-control" onChange={this.onChange} maxLength="4" minLength="4" placeholder="Enter OTP" />
                    <p className="forgot-password text-right" style={{marginTop:'8px'}}>
                        <span style={{cursor:'pointer'}} onClick={ ()=>{ this.sendOtp() } } > Resend OTP </span>
                        {/* Already registered <a href="#">sign in?</a> */}
                    </p>
                    </FormGroup>:''
                }

                <Button type="submit" className="btn btn-primary btn-block">Log In</Button>
                {
                    (this.state.passwordLogin)?
                    <p className="forgot-password text-right" style={{marginTop:'8px'}}>
                        <span style={{cursor:'pointer'}} onClick={ ()=>{ this.sendOtp() } } > Login with OTP </span>
                        {/* Forgot <a href="#">password?</a> */}
                    </p>:''
                }
                
            </Form>
        );
    }
}
