import React from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { NotificationManager } from "react-notifications";
import  postRequest  from "../../api/postRequest";
class AddEditForm extends React.Component {
    state = {
        id: (this.props.batchInfo)?this.props.batchInfo.bid:0,
        name: '',
        phone: '',
        pincode: '',
        state:'',
        email: '',
        enqType:'batch_enq',
        valid: false,
    }

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitFormAdd = async (e) => {
        e.preventDefault()
        await this.Validation()
        if(this.state.valid === true){
            const url = `api/enquiry`;
            let res = await postRequest(url , this.state);
            if(res.status === 'success'){
                this.props.toggle()
                NotificationManager.info("Service Added Successfully", 'Info', 2000);
            }else{
                NotificationManager.error("Something went wrong", 'Info', 2000);
            }
        }
    }

    Validation = () => {
        if (!this.state.name){
            return NotificationManager.error("Please Enter your Name", "Info", 2000);
        }
        else if (!this.state.phone){
            return NotificationManager.error("Please Enter Phone No.", "Info", 2000);
        }
        else if (!this.state.email){
            return NotificationManager.error("Please Enter Email", "Info", 2000);
        }
        else if (!this.state.pincode){
            return NotificationManager.error("Please Enter Pin Code", "Info", 2000);
        }
        else if (!this.state.state){
            return NotificationManager.error("Please Enter State", "Info", 2000);
        }
        else{
            this.setState({valid : true})
        }
    }

    render() {
        return (
            <Form onSubmit={this.submitFormAdd}>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" name="name" id="name" onChange={this.onChange} value={this.state.name} />
                </FormGroup>
                <FormGroup>
                    <Label for="phone">Phone</Label>
                    <Input type="number" name="phone" id="phone" onChange={this.onChange} value={this.state.phone} />
                </FormGroup>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" id="email" onChange={this.onChange} value={this.state.email} placeholder="demo@gmail.com" />
                </FormGroup>
                <FormGroup>
                    <Label for="pincode">Pincode</Label>
                    <Input type="pincode" name="pincode" id="pincode" onChange={this.onChange} value={this.state.pincode} />
                </FormGroup>
                <FormGroup>
                    <Label for="state">State</Label>
                    <Input type="select" name="state" id="state" onChange={this.onChange} value={this.state.state} >
                        <option value="">Select</option>
                        <option value="AN">Andaman &amp; Nicobar Islands</option>
                        <option value="AP">Andhra Pradesh</option>
                        <option value="AR">Arunachal Pradesh</option>
                        <option value="AS">Assam</option>
                        <option value="BI">Bihar</option>
                        <option value="CH">Chandigarh</option>
                        <option value="CG">Chhattisgarh</option>
                        <option value="DN">Dadra &amp; Nagar Haveli</option>
                        <option value="DD">Daman &amp; Diu</option>
                        <option value="DL">Delhi</option>
                        <option value="GO">Goa</option>
                        <option value="GJ">Gujarat</option>
                        <option value="HA">Haryana</option>
                        <option value="HI">Himachal Pradesh</option>
                        <option value="JK">Jammu &amp; Kashmir</option>
                        <option value="JH">Jharkhand</option>
                        <option value="KA">Karnataka</option>
                        <option value="KE">Kerala</option>
                        <option value="LK">Lakshadweep</option>
                        <option value="MP">Madhya Pradesh</option>
                        <option value="MH">Maharashtra</option>
                        <option value="MA">Manipur</option>
                        <option value="ME">Meghalaya</option>
                        <option value="MI">Mizoram</option>
                        <option value="NG">Nagaland</option>
                        <option value="OR">Orissa</option>
                        <option value="PY">Puducherry</option>
                        <option value="PJ">Punjab</option>
                        <option value="RJ">Rajasthan</option>
                        <option value="SI">Sikkim</option>
                        <option value="TN">Tamil Nadu</option>
                        <option value="TE">Telangana</option>
                        <option value="TR">Tripura</option>
                        <option value="UP">Uttar Pradesh</option>
                        <option value="UT">Uttarakhand</option>
                        <option value="WB">West Bengal</option>
                        </Input>
                </FormGroup>
                <Button color="success">Submit</Button>
            </Form>
        );
    }
}

export default AddEditForm