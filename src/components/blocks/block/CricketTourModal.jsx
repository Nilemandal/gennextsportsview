import React , { Component } from 'react';
import { Modal , ModalHeader , ModalBody , ModalFooter } from "reactstrap";
import { Button , Row , Col } from "react-bootstrap";
import {NotificationManager} from "react-notifications";
import postRequest from "../../api/postRequest";
import { dateTime , formatAMPM} from '../../helper/dateFunction';
import {env} from '../../../env';
// countDays

export default class CricketTourModal extends Component {
    state = {
        modal:false,
        tourName:this.props.tourData.tour_name,
        tourDetail:this.props.tourData.tour_detail,
        tourStart:this.props.tourData.tour_start,
        tourEnd:this.props.tourData.tour_end,
        tourLocation:JSON.parse(this.props.tourData.tour_location),
        tourCapacity:this.props.tourData.tour_capacity,
        tourAge:this.props.tourData.tour_age,
        travelDetail:this.props.tourData.travel_detail,
        tourFoods:this.props.tourData.travel_food,
        practiseSession:this.props.tourData.practise_session,
        tourMatches:this.props.tourData.tour_matches,
        coachDetail:this.props.tourData.coach_detail,
        tourPackage:this.props.tourData.tour_packge,
    }

    toggle = () => {
        this.setState((prevState) => ({
                modal: !prevState.modal,
            })
        );
    };
    
    render(){
        console.log('console the props' , this.props );

        const closeBtn = (
            <Button className="close" onClick={this.toggle}>
                &times;
            </Button>
        );

        const label = this.props.buttonLabel;
        let button = <Button color="info" onClick={this.toggle}>
                        {label}
                    </Button>;

        const __DEV__ = document.domain;
        function loadRazorPay(src) {
            return new Promise ( resolve => {
                var script =  document.createElement('script');
                script.src = src;
                script.onload = () =>{
                    resolve(true);
                }
                script.onerror = () => {
                    resolve(false);
                }
                document.body.appendChild(script);
                //script.onload = displayRazorPay;
            })
        }

        async function displayRazorPay(stateData) {
            let auth = localStorage.getItem('auth');
            if(!auth) return alert('please login before proceed');

            var res = await loadRazorPay('https://checkout.razorpay.com/v1/checkout.js');
            if(!res) return alert('Payment Failed to Start, Try Again!!');

            let serverRes = await postRequest('api/payments/razorpay/order-id',{amount:stateData.batchFees,currency:'INR',productId:stateData.batchId,product:'cricketBatch'});
            if(serverRes.status === 'success' ){
                let apiRes = serverRes.data;
                let userData = apiRes.user;

                var options = {
                    "key": __DEV__  ? 'rzp_test_9QNuoYSlMEH4Q7' : 'NOT AVAILABLE', 
                    "amount": (apiRes.amount).toString(),
                    "currency": "INR",
                    "name": "Generation Next Sports",
                    "description": stateData.batchName,
                    "image": env.serverHost+"logo.png",
                    "order_id": apiRes.orderId,
                    "handler": async function (response){
                        response['batchName'] = stateData.batchName;
                        response['batchId'] = stateData.batchId;

                        let url = 'api/payments/razorpay/response';
                        let updatePayment = await postRequest(url,response)
                        if(updatePayment.status === 'success'){
                            window.location.href = '/dashboard';
                        }else{
                            NotificationManager.error('Failed to capture payment','Payment Error',2000)
                        }
                        
                    },
                    "prefill": {
                        "name": ( userData.name)?userData.name:'',
                        "email": ( userData.email)?userData.email:'',
                        "contact": ( userData.phone)?userData.phone:'',
                    },
                    "notes": {
                        "address": "GnSports Airoli"
                    },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                var rzp1 = new window.Razorpay(options);
                rzp1.open();
            }else{
                return NotificationManager.error("Payment Response Error", "Payment Response", 2000);
            }
        }

        return(
            <React.Fragment>
                <div>
                    { button }
                    <Modal
                        isOpen={this.state.modal}
                        toggle={this.toggle}
                        className={this.props.className}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <ModalHeader style={{fontFamily:"'Acme', sans-serif"}} toggle={this.toggle} close={closeBtn}>
                            Tour Name : { this.state.tourName }
                        </ModalHeader>
                        <ModalBody className="cricket-tour-modal-body">
                            <div style={{textAlign:'center'}}>
                                { this.state.tourDetail }
                            </div>
                            <Row>
                                <Col>   
                                    <div>
                                        <p>
                                            Tour Start:  { dateTime(this.state.tourStart) } { formatAMPM(this.state.tourStart) }
                                        </p>
                                    </div>
                                    <div> 
                                        <h5> Tour Capacity & Age Group </h5>    
                                        <p> { this.state.tourCapacity } & { this.state.tourAge } </p>
                                    </div>
                                    <div>
                                        <h5> Practise Session </h5> 
                                        <p> { this.state.practiseSession } </p>
                                    </div>
                                    <div>
                                        <h5> Matches </h5>
                                        <p> { this.state.tourMatches } </p>
                                    </div>
                                    <div>
                                        <h5> Coach Detail </h5>
                                        <p> { this.state.coach_detail } </p>
                                    </div>
                                </Col>
                                <Col>
                                    <div>
                                        <p> Tour End : { dateTime(this.state.tourEnd) }{ formatAMPM(this.state.tourEnd) } </p>
                                    </div>
                                    {/* <div>
                                        <p> Cricket tour for { countDays(this.state.tourStart,this.state.tourEnd) } Days in { this.state.tourLocation.city } </p>
                                    </div> */}
                                    <div> 
                                        <h5> Tour Location </h5>
                                        <p>
                                            Ground: { this.state.tourLocation.ground } , City : { this.state.tourLocation.city } , <br/>
                                            State: { this.state.tourLocation.state }, Pincode : { this.state.tourLocation.pincode }
                                        </p> 
                                    </div>
                                    <div>
                                        <h5> Travel Detail </h5>
                                        <p> { this.state.travelDetail } </p>
                                    </div>
                                    <div>
                                        <h5> Food Detail </h5>
                                        <p> { this.state.tourFoods } </p>
                                    </div>
                                    <div>
                                        <h5> Tour package </h5>
                                        <p> { this.state.tourPackage } ₹  </p>
                                    </div>
                                </Col>
                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button variant="secondary" onClick={ this.toggle }> Close </Button>
                            <Button variant="primary" onClick={ () => { displayRazorPay(this.state) } }> Pay & Proceed </Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </React.Fragment>
        )   
    }
}