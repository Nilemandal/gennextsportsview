import React, { Component } from "react";
import ModalForm from "../model/model";

class LoginButton extends Component {
  render() {
    return (
      <div style={{ float: "right" }}>
        <ModalForm buttonLabel="Login" modal="Login"/>
      </div>
    );
  }
}

export default LoginButton;