import React, { Component } from 'react'
import { ModalBody , ModalHeader , Modal , Button } from 'reactstrap';
import { formatAMPM , weekDaysFunc } from "../../helper/dateFunction";
import postRequest from "../../api/postRequest";
import { NotificationManager } from "react-notifications";
import env from "../../../env";

export default class Info extends Component {    
    state = {
        batchId : this.props.batch.bid,
        batchName : this.props.batch.batch_name,
        batchStartTime : this.props.batch.start_time,
        batchEndTime : this.props.batch.end_time,
        batchCapacity : this.props.batch.batch_capacity,
        batchFees : this.props.batch.batch_fees,
        weekDays : this.props.batch.weekdays,
        modal : false
    }

    toggle = () => {
        this.setState((prevState) => ({
                modal: !prevState.modal,
            })
        );
    };
    
    render(){
        const closeBtn = (
            <button className="close" onClick={this.toggle}>
                &times;
            </button>
        );

        //const modalName = this.props.modal;
        const label = this.props.buttonLabel;
        let button = <Button color="info" onClick={this.toggle}>
                        {label}
                    </Button>;
        let title = this.state.batchName;
        

        const __DEV__ = document.domain;
        function loadRazorPay(src) {
            return new Promise ( resolve => {
                var script =  document.createElement('script');
                script.src = src;
                script.onload = () =>{
                    resolve(true);
                }
                script.onerror = () => {
                    resolve(false);
                }
                document.body.appendChild(script);
                //script.onload = displayRazorPay;
            })
        }

        async function displayRazorPay(stateData) {
            let auth = localStorage.getItem('auth');
            if(!auth) return alert('please login before proceed');

            var res = await loadRazorPay('https://checkout.razorpay.com/v1/checkout.js');
            if(!res) return alert('Payment Failed to Start, Try Again!!');

            let serverRes = await postRequest('api/payments/razorpay/order-id',{amount:stateData.batchFees,currency:'INR',productId:stateData.batchId,product:'cricketBatch'});
            if(serverRes.status === 'success' ){
                let apiRes = serverRes.data;
                let userData = apiRes.user;

                var options = {
                    "key": __DEV__  ? 'rzp_test_9QNuoYSlMEH4Q7' : 'NOT AVAILABLE', 
                    "amount": (apiRes.amount).toString(),
                    "currency": "INR",
                    "name": "Generation Next Sports",
                    "description": stateData.batchName,
                    "image": env.serverHost+"logo.png",
                    "order_id": apiRes.orderId,
                    "handler": async function (response){
                        response['batchName'] = stateData.batchName;
                        response['batchId'] = stateData.batchId;

                        let url = 'api/payments/razorpay/response';
                        let updatePayment = await postRequest(url,response)
                        if(updatePayment.status === 'success'){
                            window.location.href = '/dashboard';
                        }else{
                            NotificationManager.error('Failed to capture payment','Payment Error',2000)
                        }
                        
                    },
                    "prefill": {
                        "name": ( userData.name)?userData.name:'',
                        "email": ( userData.email)?userData.email:'',
                        "contact": ( userData.phone)?userData.phone:'',
                    },
                    "notes": {
                        "address": "GnSports Airoli"
                    },
                    "theme": {
                        "color": "#3399cc"
                    }
                };
                var rzp1 = new window.Razorpay(options);
                rzp1.open();
            }else{
                return NotificationManager.error("Payment Response Error", "Payment Response", 2000);
            }
        }

        return(
            <React.Fragment>
                <div>
                    {button}
                    <Modal
                        isOpen={this.state.modal}
                        toggle={this.toggle}
                        className={this.props.className}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                    <ModalHeader toggle={this.toggle} close={closeBtn}> 
                        {title}
                    </ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-sm-8 col-lg-8">
                                <p> week days : <span> {weekDaysFunc(this.state.weekDays)} </span> </p> 
                                <p> Timing : <span> {formatAMPM(this.state.batchStartTime)} </span> to {formatAMPM(this.state.batchEndTime)} </p>                            
                                <p> Batch Capacity : <span> {this.state.batchCapacity} </span> </p>                                 
                                <p> Batch Fees : <span> {this.state.batchFees} </span> </p>                                 
                            </div>
                            <div className="col-sm-4 col-lg-4">
                                <p> 30 Days course Duration </p> 
                                <p> Batch Fess : <span> {this.state.batchFees} </span> </p>
                                <Button onClick={ ()=>{ displayRazorPay(this.state) } } style={{marginTop:'5px', display: 'flex', justifyContent: 'center'}} > Pay & Activate </Button>
                            </div>
                        </div>
                    </ModalBody>                    
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}