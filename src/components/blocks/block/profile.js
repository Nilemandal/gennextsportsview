import React , {useState } from "react";
import './block.css';
import { Modal , Form , Button } from "react-bootstrap";
import { NotificationManager } from "react-notifications";
import postRequest from "../../api/postRequest";
export default class Profile extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            user:this.props.user,
            modal:false
        }

        this.auth = localStorage.getItem('auth');
        if(!this.auth){
            // NotificationManager.error('Please Login to Proceed with Dashboard','Info',2000);
            alert('Please Login to Proceed with Dashboard');
            setTimeout(()=>{
                window.location = '/';
            },1000);
        }
    }

    toggle = async () => {
        this.getUserData = await postRequest('api/user/data');
        this.setState((prevState) => ({
            modal: !prevState.modal,
            user:this.getUserData.data
        }));
    };

    render(){
        let user = (this.state.user)?this.state.user:this.props.user;
        let userStatus = false;
        if(user && user.user_name){
            userStatus = true;
        }

        let modalForm = '';
        if(this.state.modal){
            modalForm = <ProfileModal toggle={this.toggle} user={user}  modal={this.state.modal}/>
        }

        return(
            <React.Fragment>
                <div className="profile-id animated fadeIn">
                    <div className="card">
                        <div className="card-body">
                            <div className="avatar">
                                {/* <img 
                                    src="imgs/logo1.png"
                                    className="card-img-top"
                                    alt=""
                                /> */}
                                <svg xmlns="http://www.w3.org/2000/svg" width="90" height="90" fill="currentColor" className="bi bi-person" viewBox="0 0 16 16">
                                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                </svg>
                            </div>

                            <div className="card-title">
                                { userStatus && user.user_name }
                            </div>

                            <p>
                                { userStatus && user.user_phone } 
                                <br/>
                                { userStatus && user.user_email }
                                <br/>
                                { userStatus && user.location }
                                <br/>
                                { userStatus && user.dob }
                            </p>
                            
                        </div>
                        <div style={{textAlign:"right"}} onClick={ ()=> { this.toggle() } }>
                            Edit &nbsp; &nbsp;
                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                            </svg>
                        </div>
                        
                        <div>
                            { 
                                modalForm 
                            }
                        </div>
                    </div>
                </div> 
            </React.Fragment>
        );
    }
}

const ProfileModal = ({toggle , modal , user}) => {
    // let { user_email , user_name , user_phone  } = user;
    const [ userData , setUserData ] = useState(user);
    const [ otpStatus , setOtpStatus ] = useState(false);
    const inpChange = (e) =>{
        setUserData({
            ...userData,
            [e.target.name]:e.target.value,
        })
    }

    // useEffect(() => {
    //     console.log( 'console the user data' , userData);
    // }, [userData]);

    const submitForm = async (e) => {
        e.preventDefault();
        let flag = await validateForm();
        if(flag){
            if(otpStatus){
                let password = (userData.passKey1)?userData.passKey1:'';
                let updateJson = {
                    ...userData,
                    password,
                }

                const url = 'api/user/profile-update';
                let res = await postRequest(url,updateJson);
                if(res.status === 'success'){
                    toggle();
                }else if(res.code === 1){
                    return NotificationManager.error("OTP Miss Match Please Try Again", "Info", 2000);
                }

            }else{
                const otpUrl = 'api/send-otp-phone';
                let otpRes = await postRequest(otpUrl,{phone:userData.user_phone});
                if(otpRes.status === 'success'){
                    setOtpStatus(true);
                }else{
                    return NotificationManager.error("Something went wrong Try Again", "Info", 2000);
                }
            }
        }
    }

    function validateForm(){
        let flag = false;
        return new Promise( (resolve,rejects)=>{
            if(userData.user_name && (/^[A-z ']+$/.test(userData.user_name)) ){
                flag = true;
            }else{
                NotificationManager.error("Please Enter your Name", "Info", 2000);
            }
    
            if(userData.user_email &&  (/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(userData.user_email)) ){
                flag = true;
            }else{
                NotificationManager.error("Please Check Email", "Info", 2000);
            }

            if(userData.user_phone && (/^\d{10}$/.test(userData.user_phone)) ){
                flag = true;
            }else{
                NotificationManager.error("Please Check Phone No.", "Info", 2000);
            }
    
            if( userData.passKey1 && userData.passKey1 !== userData.passKey2 ){
                flag = false;
                NotificationManager.error("Please Match Password", "Info", 2000);
            }

            if( otpStatus && ( !(/^\d{4}$/.test(userData.otp))) ){
                flag = false;
                NotificationManager.error("Please Check 4 Digit Otp", "Info", 2000);
            }

            resolve(flag);
        });
    }

    return(
        <Modal
            size="sm"
            show={ modal }
            onHide={() => { toggle() } }
            aria-labelledby="profile-edit-modal"
        >
            <Modal.Header closeButton>
                <Modal.Title id="profile-edit-modal">
                    Profile Update
                </Modal.Title>
            </Modal.Header>
            <Form onSubmit={submitForm}>
                <Modal.Body>
                    {
                        !otpStatus
                        &&
                        <React.Fragment>
                            <Form.Group controlId="formGroupName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter Name" name="user_name" onChange={ inpChange } defaultValue={userData.user_name} />
                            </Form.Group>
                            <Form.Group controlId="formGroupEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" name="user_email" onChange={ inpChange } defaultValue={userData.user_email} />
                            </Form.Group>
                            <Form.Group controlId="formGroupPhone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control type="number" placeholder="Enter Phone" name="user_phone" onChange={ inpChange } defaultValue={userData.user_phone} readOnly/>
                            </Form.Group>
                            <Form.Group controlId="formGroupPassword1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="********" name="passKey1" onChange={ inpChange }  />
                            </Form.Group>
                            <Form.Group controlId="formGroupPassword2">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control type="password" placeholder="********" name="passKey2" onChange={ inpChange } />
                            </Form.Group>
                        </React.Fragment>
                    }
                    {
                        otpStatus
                        &&
                        <Form.Group controlId="formGroupOtp">
                            <Form.Label>Otp</Form.Label>
                            <Form.Control type="number" placeholder="****" name="otp" onChange={ inpChange } />
                        </Form.Group>
                    }
                    {
                        false 
                        &&
                        <React.Fragment>
                            <Form.Group controlId="formGroupPinCode">
                                <Form.Label>Pin Code</Form.Label>
                                <Form.Control type="text" placeholder="Pin Code" readOnly />
                            </Form.Group>
                            <Form.Group controlId="formGroupDOB">
                                <Form.Label> Date of Birth </Form.Label>
                                <Form.Control type="date" placeholder="" readOnly />
                            </Form.Group>
                        </React.Fragment>
                    }
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success" type="submit" >Submit</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}