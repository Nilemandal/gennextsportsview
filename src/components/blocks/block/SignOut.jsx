import React, { Component } from "react";
import NotificationManager from "react-notifications/lib/NotificationManager";
import postRequest from "../../api/postRequest";

export default class SignOut extends Component {
  async SignOut(){
    const url = `api/user/logout`;
    let res = await postRequest(url);

    if(res.status === 'success'){
      localStorage.setItem('auth','');
      NotificationManager.success('User Sign Out Successfully','Sign Out', 2000)
      window.location.reload();
    }else{
      if(res.msg === 'Session Not Found'){
        localStorage.setItem('auth','');
        NotificationManager.success('User Sign Out Successfully','Sign Out', 2000)
        window.location.reload();
      }else
        NotificationManager.error('Something Went Wrong Try Again','Sign Out Error', 2000)
    }
  }

  render() {
    return (
      <div style={{ float: "right" }}>
        <span onClick={ this.SignOut }> Sign Out </span>
      </div>
    );
  }
}
