import React from 'react';
import postRequest from '../../api/postRequest';
import { Accordion , Card , ProgressBar } from "react-bootstrap";
import { monthYear } from "../../helper/dateFunction";

export default class CricketBatch extends React.Component {
    constructor(props){
        super(props)
        this.state = { user: {} , subscribedData:null };
    }

    componentDidMount(){
        this.setState({
            user:this.props.user
        }, ()=>{
            this.getSubscribedData();
        })
    }

    getSubscribedData = async ()=> {
        const url = '/api/dashboard/subcribed-data'
        let res = await postRequest(url,{user:this.state.user});
        if(res.status === 'success'){
            this.setState({
                subscribedData:res.data
            })
        }
    }

    render(){
        let el = undefined;
        if(this.state.subscribedData && this.state.subscribedData.length > 0){
            el = this.state.subscribedData.map( (batch,index) => {
                return(
                    <Card key={'card_'+index}>
                        <Accordion.Toggle as={Card.Header} eventKey={index+1}>
                            {batch.batch_name} &nbsp; &nbsp; { monthYear(batch.timestamp) } - { monthYear(batch.batch_expires) }
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey={index+1}>
                            <Card.Body>
                                {   Object.keys(JSON.parse(batch.performance)).length > 0 ?
                                    Object.keys(JSON.parse(batch.performance)).map(
                                        (keys , index)=> {
                                            return <PerformnaceCard key={'card_'+index} performnaceCard data={JSON.parse(batch.performance)[keys]} month={keys} index={index} />
                                        }
                                    )
                                    :
                                    <h5> Performance Not Updated. </h5>
                                }
                            </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                )
            })
        }

        return(
            <div>
                <Accordion defaultActiveKey="">
                    {el}
                </Accordion>
            </div>
        )
    }
}

const PerformnaceCard = (props) =>{
    let data = props.data;
    let length = props.index;
    let month = props.month.split('_')[0];
    let year = props.month.split('_')[1];

    return(
        <Card key={'per_card_'+length} style={{width: '100%'}}>
            <h3> { month } { year } </h3>
            <Card.Body>
                Batting
                <ProgressBar variant="success" now={data.batting} label={`${data.batting}%`} /> <br/> 
                Bowling
                <ProgressBar variant="info" now={data.bowling} label={`${data.bowling}%`} /> <br/>
                Fielding
                <ProgressBar variant="warning" now={data.fielding} label={`${data.fielding}%`} /> <br/>
                Attendance
                <ProgressBar variant="danger" now={data.attendance} label={`${data.attendance}%`} /> <br/>
            </Card.Body>
        </Card>
    )
}