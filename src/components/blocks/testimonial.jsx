import React , { useState , useEffect } from 'react';
import { Carousel , Container } from "react-bootstrap";
import getRequestData from "../api/getRequest";
import { dateTime } from '../helper/dateFunction'
import "./blocks.css";

const Testimonial =  () => {
    const [ testimonials , setTestimonial ] = useState([]);
    useEffect(() => {
        getTestimonials();
    }, [])
    
    const getTestimonials = async () => {
        const url = 'api/get-testimonial';
        let res = await getRequestData(url);
        if(res.status === 'success' && res.data[0] ){
            setTestimonial(res.data)
        }
    }

    if(testimonials[0]){
        return(
            <Container className="testimonial-sections">
                <hr/>
                <h1 className="display-5 text-center">
                    Testimonials
                </h1>
                <hr/>
                <Carousel className="text-center">
                    {
                        testimonials.map( (d,i) => {
                            return(
                                <Carousel.Item key={'item_'+i} >
                                    <div className="mb-3">
                                        <img src="imgs/profile-dummy.png" alt="" className="cricle-img-style"/>
                                        <br/>
                                        <p> 
                                            { 
                                                [1,2,3,4,5].map( (x,i)=>{
                                                    if(i <= d.cus_ratings){
                                                        return <span className="fa fa-star checked" key={"ratings_"+i+"_"+d.timestamp}>  </span> 
                                                    }else{
                                                        return <span className="fa fa-star" key={"ratings_"+i+"_"+d.timestamp} >  </span>
                                                    }
                                                })
                                            }
                                        </p>
                                        <p>
                                        Review : { d.cus_feedback }
                                        </p>
                                        <p>
                                            Name : {d.cus_name} <br/>
                                            Profession : {d.cus_profession} <br/>
                                            Date : { dateTime(d.timestamp) }
                                        </p>
                                        <br/>
                                        
                                    </div>
                                </Carousel.Item>
                            )
                        })
                    }
                </Carousel>
            </Container>
        ) 

    }else{
        return(
            <React.Fragment> </React.Fragment>
        );
    }
}

export default Testimonial;