// import { CButton } from '@coreui/react'
import React, { Component } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import AddEditForm from "../Form/enquiryForm";
import Login from "../Form/loginForm";
import SignUp from "../Form/signupForm";
class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
  }

  toggle = () => {
    this.setState((prevState) => ({
      modal: !prevState.modal,
    }));
  };

  render() {
    const closeBtn = (
      <button className="close" onClick={this.toggle}>
        &times;
      </button>
    );
    
    const modalName = this.props.modal;
    const label = this.props.buttonLabel;
    let ModalForm = '';
    if(modalName === 'Enquiry'){
      ModalForm = <AddEditForm toggle={this.toggle} batchInfo={this.props.batchInfo} />;
    }else if(modalName === 'Login'){
      ModalForm = <Login toggle={this.toggle} />;
    }else if(modalName === 'Signup'){
      ModalForm = <SignUp toggle={this.toggle} />
    }

    let button = "";
    let title = "";

    if (label === "Enquiry") {
      button = (
        <Button color="info" onClick={this.toggle}>
          {label}
        </Button>
      );
      title = "Enquiry Form";
    }else if( label === 'Login' ){
      button = (
        <span color="info" onClick={this.toggle}>
          {label}
        </span>
      );
      title = "Login";
    }else if( label === 'Register' ){
      button = (
        <span color="info" onClick={this.toggle}>
          {label}
        </span>
      );
      title = "Register";
    }

    return (
      <div>
        {button}
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle} close={closeBtn}>
            {title}
          </ModalHeader>
          <ModalBody>
            {ModalForm}
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default ModalForm;
