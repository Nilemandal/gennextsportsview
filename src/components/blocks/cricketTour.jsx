import React , { useState , useEffect } from "react";
import { Container , Carousel , Card } from "react-bootstrap";
import { formatAMPM , dateTime , countDays } from '../helper/dateFunction';
import postRequest from "../api/postRequest";
import CricketTourModal from "./block/CricketTourModal";
import "./blocks.css";

const CrirketTour = () => {
    const [ tourData , setTourData ] = useState([]);
    
    useEffect( ()=> {
        getTourActivity();
    },[])

    const getTourActivity = async () => {
        const url = 'api/tour/fetch-tour-detail';
        let res = await postRequest(url);
        if(res.status === 'success' && res.data[0] ){
            setTourData(res.data)
        }
    }

    const getLocationData = (obj,dataField) => {
        return JSON.parse(obj)[dataField];
    }

    return(
        <Container className="cricket-tour-section">
            <hr/>
                <h1 className="display-5 text-center">
                    Cricket Tour
                </h1>
            <hr/>
            <Carousel>
                {
                    tourData.map( (tour , index) => {
                        return(
                            <Carousel.Item key={ "tour_"+(index+1) }>
                                <Card>
                                    <Card.Header> 
                                        <span style={{fontSize:'35px'}}> {tour.tour_name} </span> 
                                    </Card.Header>
                                    <Card.Body>
                                        <Card.Title style={{fontSize:"25px"}}>
                                            <p>
                                                Cricket Tour to <span className="text-info"> {getLocationData(tour.tour_location,'city')} </span>  for <span className="text-info"> { countDays(tour.tour_start,tour.tour_end) } </span>  Days
                                            </p>
                                            <p>
                                                Date & Time <span className="text-info"> { dateTime(tour.tour_start)} {formatAMPM(tour.tour_start) } </span> to <span className="text-info"> { dateTime(tour.tour_end)} { formatAMPM(tour.tour_end)} </span>
                                            </p>
                                            <p>
                                                Age Group <span className="text-info">  { tour.tour_age } </span>
                                            </p>
                                        </Card.Title>
                                        <Card.Text>
                                            <span className="text-secondary" style={{fontSize:"20px"}}> {tour.tour_detail} </span>
                                        </Card.Text>
                                        <CricketTourModal buttonLabel="Tour Details" className="btn-style enq-btn" size="sm" tourData={tour}>Tour Details</CricketTourModal>
                                    </Card.Body>
                                </Card>
                            </Carousel.Item>
                        )
                    })
                }
            </Carousel>
        </Container>
    )
}

export default CrirketTour;