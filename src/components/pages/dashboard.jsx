import React , { Component } from "react";
import postRequest from '../api/postRequest';
import { Container , Row , Col } from "react-bootstrap";
import Profile from "../blocks/block/profile";
import CricketBatch from "../blocks/block/CricketBatch";

export default class Dashboard extends Component{
    auth = localStorage.getItem('auth');
    user = localStorage.getItem('user');

    state = {
        page:'dashboard',
        userData: null
    };

    checkAuth = async ()=>{
        if(!this.auth) window.location.href = '/';
    }

    getUser = async ()=>{
        let getUserData = await postRequest('api/user/data');
        if(getUserData.status === 'success') {
            this.setState({userData:getUserData.data})
        }
    }

    componentDidMount(){
        this.getUser();
    }

    render(){
        return(
            <React.Fragment>
                <Container fluid style={{ marginTop : '15px' }}>
                    <Row>
                        <Col sm={3}> 
                            <Profile user={this.state.userData} />
                        </Col>
                        <Col sm={9}> 
                            {
                                this.state.userData !== null ? 
                                <CricketBatch user={this.state.userData} />  :
                                <React.Fragment>  </React.Fragment>
                            }
                        </Col>
                    </Row>
                </Container>

                {/* <h1> DashBoard Redirect {(user && user.user_name)?user.user_name:""}  </h1> */}
            </React.Fragment>
        )
        
    }
}
