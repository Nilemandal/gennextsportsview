import React from "react";
import Promotionalbanner from "../blocks/promotionalBanner";
import SportsSession from "../blocks/SportsSession";
import CricketTour from "../blocks/cricketTour";
import Testimonial from "../blocks/testimonial";

const Home = () =>{
    return(
        <div className="container-fluid">
            <Promotionalbanner />
            <SportsSession />
            <CricketTour />
            <Testimonial />
        </div>
    );
};

export default Home;