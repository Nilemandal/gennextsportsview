import React , {useState} from "react";
import BatchUpdationComponent from "../../admin/batch-detail/cricket-batch";
import AdimNavigation from '../../admin/navigation';
import Performance from '../../admin/performance/Performance';
import CricketTour from "../../admin/cricket-tour/cricketTour";

const AdminPage = () => {
    const [ componentStatus , setComponentStatus ] = useState({
        'batchStatus':true,
        'batchPerformance':false,
        'cricketTourStatus':false
    })

    const changeComponents = (componentsName) => {
        let newObj = {};
        for(const key in componentStatus){
            newObj[key] = false
        }
        newObj[componentsName] = true;
        setComponentStatus(newObj);
    }

    return(
        <React.Fragment>
            
            <AdimNavigation handleChange={ changeComponents } />

            {
                (componentStatus.batchStatus)?
                <BatchUpdationComponent />:<React.Fragment></React.Fragment>
            }
            {
                (componentStatus.batchPerformance)?
                <Performance /> : <React.Fragment> </React.Fragment>
            }
            {
                (componentStatus.cricketTourStatus)?
                <CricketTour />: <React.Fragment> </React.Fragment>
            }
        </React.Fragment>
    )
}

export default AdminPage;