import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { SocialIcon } from "react-social-icons";
import "./footer.css";

function Footer() {
  return (
    <div className="App">
      <div id="footer-wrap">
        <footer className="footer">
          <div className="top-footer">
            <div className="row">
              <div className="col-md-3">
                <div className="footer-logo">
                  <a href="/" title="Mercury">
                    <img
                      src="/imgs/logo1.png"
                      width="72"
                      alt="Mercury-Logo"
                      className="img-fluid"
                    />
                    Generation Next
                  </a>
                </div>
              </div>
              <div className="col-md-2">
                <h4>Short Links</h4>
                <ul className="footer-link">
                  <li>
                    <a href="/" title="Home">
                      Home
                    </a>
                    <a href="/about" title="About">
                      About Us
                    </a>
                    <a href="/contact" title="Contact">
                      Contact Us
                    </a>
                    <a href="/sports" title="Sports">
                      Sports Wear
                    </a>
                  </li>
                </ul>
              </div>

              <div className="col-md-2">
                
              </div>

              <div className="col-md-3">
                <h4>Contact Us</h4>
                <ul className="footer-link">
                  <li>
                      <p>Aaroli,Navi mumbai,Maharashtra.</p>
                      <p>7738225710 | 9819869803 </p>
                    <a href="mail-to:smartlight.eip@gmail.com" title="Contact">
                      generationext@info.com | gnsportsclub@gmail.com
                    </a>
                  </li>
                  <li>
                    <div className="icons">
                      <SocialIcon
                        title="Facebook"
                        network="facebook"
                        bgColor="#ffffff"
                        url="https://www.facebook.com/Generation-Next-725076084519838/"
                        target="_blank"
                      />
                      <SocialIcon
                        title="Instagram"
                        network="instagram"
                        bgColor="#ffffff"
                        target="_blank"
                        url="https://www.instagram.com/generationnextcricketacademy/"
                      />
                      <SocialIcon
                        title=""
                        network=""
                        bgColor="#ffffff"
                        target="_blank"
                        url="https://api.whatsapp.com/send?phone=7738225710"
                      />
                      <SocialIcon
                        title=""
                        network=""
                        bgColor="#ffffff"
                        target="_blank"
                        url="https://www.linkedin.com/"
                      />
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          
          <div className="bottom-footer">
            <div className="row">
              <div className="col-md-5">
                <p className="copyright pt-3">
                  Copyright &copy; 2021 Generation Next | All Right Reserved 
                </p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
  );
}


export default Footer