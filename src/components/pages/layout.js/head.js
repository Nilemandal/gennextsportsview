import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from 'react-bootstrap/Navbar'
import {Link} from "react-router-dom";
import "./head.css";
import LoginButton from "../../blocks/block/login";
import SignUp from "../../blocks/block/signup";
import SignOut from "../../blocks/block/SignOut";

class Header extends Component{
    state = {
        auth : localStorage.getItem('auth')
    }

    handleScroll = () => {
        window.scroll({
            top: document.body.offsetHeight,
            left: 0, 
            behavior: 'smooth',
        });
    }

    render(){
        return(
                <Navbar collapseOnSelect expand="lg" style={{backgroundColor:"#3399ff", color:""}}>
                    <img src="/imgs/logo1.png"  className="logo"alt="logo"/>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <Link className="nav-link" to="/">Home </Link>
                                </li>
                                {/* <li className="nav-item ">
                                    <Link className="nav-link" to="/about">About us</Link>
                                </li> */}
                                {/* <li className="nav-item ">
                                    <Link className="nav-link" to="/sports">Sports</Link>
                                </li> */}
                                {/* <li className="nav-item ">
                                    <Link className="nav-link" to="/clothes">Clothes</Link>
                                </li> */}
                                <li className="nav-item ">
                                    <Link className="nav-link" onClick={this.handleScroll} to="">Contact us </Link>
                                </li>
                                {
                                    (this.state.auth)?
                                        <React.Fragment>
                                            <li className="nav-item ">
                                                <Link className="nav-link" to="/dashboard"> Dashboard </Link>
                                            </li>
                                            <li className="nav-item justify-text-right" style={{"marginTop":"8px","marginLeft":"8px",'cursor': 'pointer'}}>
                                                <SignOut />
                                            </li> 
                                        </React.Fragment>
                                    :
                                        <React.Fragment>
                                            <li className="nav-item text-right" style={{"marginTop":"8px","marginLeft":"8px",'cursor': 'pointer'}}>
                                                <LoginButton /> 
                                            </li>
                                            <li className="nav-item text-right" style={{"marginTop":"8px","marginLeft":"8px",'cursor': 'pointer'}}>
                                                <SignUp />
                                            </li>
                                        </React.Fragment>
                                }
                            </ul>
                        </Navbar.Collapse>
                </Navbar>
        );
    }
}

export default Header;