import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./footer.css";

const Footer = () =>{
    return(
        <div className="main-footer">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h4>Contact No</h4>
                        <ul className="list-unstyled">
                            <li>
                                7208851122
                            </li>
                        </ul>
                    </div>

                    <div className="col">
                        <h4>address</h4>
                        <ul className="list-unstyled">
                            <li>
                                Aaroli,Navi mumbai,Maharashtra.
                            </li>
                        </ul>
                    </div>

                    <div className="col">
                        <h4>social link</h4>
                        <ul className="list-unstyled">
                            <li>
                                Social links
                            </li>
                        </ul>
                    </div>
                </div>

                <hr style={{border: '1px solid', color:'white'}} />
                <div className="row">
                    <p className="col-sm">
                        &copy;2021 Generation Next| All Right Reserved 
                    </p>
                </div>
            </div>
        </div>
        );
};

export default Footer;

