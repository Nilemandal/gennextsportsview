import React from "react";
import { Container } from "react-bootstrap";
// import './style/'

const AboutUs = () =>{
    return(
        <Container className="about-us-intro" fluid>
            <h1> About Us </h1>
        </Container>
    );
};

export default AboutUs;