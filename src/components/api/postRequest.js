import axios from "axios";
import NotificationManager from "react-notifications/lib/NotificationManager";
import {serverHost} from '../../env';

// const axiosInstance = axios.create({
//     baseURL: 'http://localhost:3001/'
// });

let config = {
    headers: {
      'Content-Type': 'application/json',
      'authorization': localStorage.auth
    }
};

const postRequest = async (url , data)=> {
    return new Promise( async (resolve,rejects)=>{
      axios.post( serverHost+url , data, config )
      .then(response =>{
        if(response.status === 200){
          let data = response.data;
          if(data.task === 'logout'){
            NotificationManager.error('Authentication failure, Please Login Again',' Authentication Error', 2000)
            setTimeout(() => {
              localStorage.setItem('auth','');
              window.location.reload();
            }, 2000);
          }else resolve(data)
        }else NotificationManager.error('Some felt wrong at Network Side','NETWORK Failure', 2000);
      })
    });
}


// const postRequest = async (url,data) => {
//   return new Promise( async (resolve,rejects) =>{
//     fetch(host+url, {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify(data),
//       })
//       .then(response => response.json())
//       .then(data => {
//         console.log('Success:', data);
//       })
//       .catch((error) => {
//         console.error('Error:', error);
//       });
//   })
// }

export default postRequest;