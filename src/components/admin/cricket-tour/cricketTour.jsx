import React from "react";
import { Form , Row , Col , Button } from "react-bootstrap";
import NotificationManager from 'react-notifications/lib/NotificationManager';
import postRequest from "../../api/postRequest";

class CricketTour extends React.Component {
    rawObj = {
        tourName:'', 
        tourDetail:'',
        ground:'', 
        city:'',
        pincode:'',
        state:'',
        tourCapacity:'',
        tourGroup:'', 
        travelDetail:'',
        travelFoods:'',
        practiseSession:'',
        tourMatch:'',
        coachDetail:'',
        tourPackage:'',
        tourStatus:'',
        tourStartDate:'',
        tourStartTime:'',
        tourEndDate:'',
        tourEndTime:''
    }

    handleChange(e){
        this.rawObj[e.target.name] = (e.target.value).trim();
    }

    async handleSubmit(obj){
        let flag = await this.validateForm(obj);
        if(!flag) return;
        let processedObj = await this.processedObj(obj);
        const url = 'api/tour/add-activity';
        let res = await postRequest(url , JSON.stringify(processedObj) );
        if(res.status === 'success') NotificationManager.info(`Tour Detail inserted Successfully Tour id is ${res.id}`,'Info',2000);
        else NotificationManager.error('oops something went wrong at server side','Error',2000);
    }

    processedObj = async (obj) => {
        return new Promise( (resolve,rejects) => {
            let sDate = (obj.tourStartDate).split('-');
            let sTime = (obj.tourStartTime).split(':');
            let eDate = (obj.tourEndDate).split('-');
            let eTime = (obj.tourEndTime).split(':');

            const startTime = +new Date(sDate[0],(sDate[1]-1),sDate[2]).setHours(sTime[0],sTime[1],0);
            const endTime = +new Date(eDate[0],(eDate[1]-1),eDate[2]).setHours(eTime[0],eTime[1],0);
        
            obj['startTime'] = startTime;
            obj['endTime'] = endTime;

            obj['tourLocation'] = {
                'ground':obj['ground'],
                'city':obj['city'],
                'pincode':obj['pincode'],
                'state':obj['state']
            }
            resolve(obj)
        });
    }

    validateForm = async (obj) => {
        let flag = true;

        let errMsg = {
            tourName:'Tour Name',
            tourDetail:'Tour Detail',
            ground:'Ground',
            city:'City',
            pincode:'Pin Code',
            state:'State',
            tourCapacity:'Tour Capacity',
            tourGroup:'Tour Group', 
            travelDetail:'Travel Detail',
            travelFoods:'Travel Foods',
            practiseSession:'Practise Session',
            tourMatch:'Tour Match',
            coachDetail:'Coach Detail',
            tourPackage:'Tour Package',
            tourStatus:'Tour Status',
            tourStartDate:'Tour Start Date',
            tourStartTime:'Tour Start Time',
            tourEndDate:'Tour End Date',
            tourEndTime:'Tour End Time'
        }

        return new Promise( (resolve,rejects) => {
            for( const key in obj){
                if(obj[key] === ""){
                    flag = false;
                    NotificationManager.error(`${errMsg[key]} is empty`,'Error',2000);
                }
            }
            resolve(flag)
        })
    }

    render() {

        return(
            <React.Fragment>
                <div className="container">
                    <Form>
                        <Row>
                            <Col>
                                <h3> Cricket Tour </h3>
                                <Form.Group controlId="tourName">
                                    <Form.Label>Tour Name</Form.Label>
                                    <Form.Control type="text" name="tourName" onChange={ (e) => this.handleChange(e) } placeholder="Enter Name" />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label> Tour Detail </Form.Label>
                                    <Form.Control type="textarea" name="tourDetail" onChange={ (e) => this.handleChange(e) } placeholder="Enter Tour Detail"/>  
                                </Form.Group>
                                <Form.Group>
                                    <h5> Tour Location </h5>
                                    <Row>
                                        <Col>
                                            <Form.Label> Ground </Form.Label>
                                            <Form.Control type="text" name="ground" onChange={ (e) => this.handleChange(e) } placeholder="Enter Ground Name" /> 
                                        </Col>
                                        <Col>
                                            <Form.Label> City </Form.Label>
                                            <Form.Control type="text" name="city" onChange={ (e) => this.handleChange(e) } placeholder="Enter City Name" /> 
                                            
                                        </Col>
                                    </Row>
                                    <Row className="mt-2">
                                        <Col>
                                            <Form.Label> Pin Code </Form.Label>
                                            <Form.Control type="number" name="pincode" onChange={ (e) => this.handleChange(e) } placeholder="Enter Pin code" /> 
                                        </Col>
                                        <Col>
                                            <Form.Label> State </Form.Label>
                                            <Form.Control type="text" name="state" onChange={ (e) => this.handleChange(e) } placeholder="Enter State Name" /> 
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <h5> Start Data n Time </h5>
                                    <Row>
                                        <Col>
                                            <Form.Label> Select Date </Form.Label> 
                                            <Form.Control type="date" name="tourStartDate" onChange={ (e) => this.handleChange(e) } />
                                        </Col>
                                        <Col>
                                            <Form.Label> Select Time </Form.Label>
                                            <Form.Control type="time" name="tourStartTime" onChange={ (e) => this.handleChange(e) } />
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <h5> End Date n Time </h5>
                                    <Row>
                                        <Col>
                                            <Form.Label> Select Date </Form.Label>
                                            <Form.Control type="date" name="tourEndDate" onChange={ (e) => this.handleChange(e) } />
                                        </Col>
                                        <Col>
                                            <Form.Label> Select Time </Form.Label>
                                            <Form.Control type="time" name="tourEndTime" onChange={ (e) => this.handleChange(e) } />
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group> 
                                    <Row>
                                        <Col>
                                            <Form.Label> Tour Capacity </Form.Label>
                                            <Form.Control type="text" name="tourCapacity" onChange={ (e) => this.handleChange(e) } placeholder="e.g. 15 Members" />
                                        </Col>
                                        <Col>
                                            <Form.Label> Tour Group </Form.Label>
                                            <Form.Control type="text" name="tourGroup" onChange={ (e) => this.handleChange(e) } placeholder="e.g. Under 14" />
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col> 
                                            <Form.Label> Travel Detail </Form.Label>
                                            <Form.Control as="textarea" name="travelDetail" onChange={ (e) => this.handleChange(e) } rows={2} />
                                        </Col>
                                        <Col> 
                                            <Form.Label> Travel Food </Form.Label> 
                                            <Form.Control as="textarea" name="travelFoods" onChange={ (e) => this.handleChange(e) } rows={2} />
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col>
                                            <Form.Label> Practise Session </Form.Label>
                                            <Form.Control as="textarea" name="practiseSession" onChange={ (e) => this.handleChange(e) } rows={2} />
                                        </Col>
                                        <Col>
                                            <Form.Label> Tour Matches </Form.Label>
                                            <Form.Control as="textarea" name="tourMatch" onChange={ (e) => this.handleChange(e) } rows={2} />
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col>
                                            <Form.Label> Coach Detail </Form.Label>
                                            <Form.Control as="textarea" name="coachDetail" onChange={ (e) => this.handleChange(e) } rows={2} />
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col>
                                            <Form.Label> Tour Package </Form.Label>
                                            <Form.Control type="number" name="tourPackage" onChange={ (e) => this.handleChange(e) } />
                                        </Col>
                                        <Col>
                                            <Form.Label> Tour Status </Form.Label>
                                            <Form.Control as="select" name="tourStatus" onChange={ (e) => this.handleChange(e) }>
                                                <option>  </option>
                                                <option value="active"> active </option>
                                                <option value="inActive"> inActive </option>
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <Row> 
                                        <Col>
                                            <Button onClick={ ()=> { this.handleSubmit(this.rawObj) } }> Submit </Button>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Col>
                            <Col>
                            
                            </Col>
                        </Row>
                    </Form>
                </div>
            </React.Fragment>
        )
    }
}

export default CricketTour;