import React , { useState , useEffect }  from "react";
import getRequestData from "../../api/getRequest";
import { formatAMPM } from "../../helper/dateFunction";
import '../admin.css';
import postRequest from "../../api/postRequest";
import NotificationManager from 'react-notifications/lib/NotificationManager';

const BatchUpdationComponent = () => {
    // const [ newBatchData , setNewBatchData ] = useState({});
    const [ batchData, setBatchData ] = useState({})
    const [ getError , setError ] = useState(true);

    useEffect( () => {

        let getFetch = async () => {
            let res = await getRequestData('api/get-batches');
            if( res.status === 'success'){
                setBatchData(res.data);
                setError(false);
            }else{
                setError(true);
            }
        }
        getFetch();
    }, []);

    let rawObj = {};
    let weekdays = {
        'sunday':1,
        'monday':2,
        'tuesday':3,
        'wednesday':4,
        'thursday':5,
        'friday':6,
        'saturday':7
    }
    let weekArray = [];

    const handleChange = (e) => {
        if( weekdays[e.target.name] && e.target.checked ) {
            weekArray.push(weekdays[e.target.name]);
            rawObj['weekDays'] = weekArray;
        }
        else rawObj[e.target.name] = e.target.value;
    }

    const formSubmit = async () => {
        let flag = await validateForm(rawObj);
        if(!flag) return;
        let processedObj = await processObject(rawObj);

        const url = 'api/add-batches-detail';
        let res = await postRequest(url , JSON.stringify(processedObj) );
        if(res.status === 'success') NotificationManager.error(`Batch Detail inserted Successfully Batch id is ${res.insertId}`,'Error',2000);
        else NotificationManager.error('oops something went wrong at server side','Error',2000);
    }

    function validateForm(obj){
        let flag = true
        if(!obj.batchName){
            NotificationManager.error('Batch Name field is empty','Error',2000);
            flag = false;
        }

        if(!obj.setHourStartTime){
            NotificationManager.error('Start Time Set Hour is Empty','Error',2000);
            flag = false
        }

        if(!obj.setMinuteStartTime){
            NotificationManager.error('Start Time Set Min is Empty','Error',2000);
            flag = false
        }

        if(!obj.setHourEndTime){
            NotificationManager.error('End Time set Hour is Empty','Error',2000);
            flag = false
        }

        if(!obj.setMinuteEndTime){
            NotificationManager.error('End Time Set Min is Empty','Error',2000);
            flag = false
        }

        if(!obj.batchFees){
            NotificationManager.error('Batch Fees is Empty','Error',2000);
            flag = false
        }

        if(!obj.batchCapacity){
            NotificationManager.error('Batch Capacity is Empty','Error',2000);
            flag = false
        }

        if(!obj.fitnessDetail){
            NotificationManager.error('Fitness Detail is Empty','Error',2000);
            flag = false
        }

        if(!obj.batchStatus){
            NotificationManager.error('Batch Status is not Selected','Error',2000);
            flag = false
        }

        return flag;   
    }

    function processObject(obj){

        const startTime = +new Date().setHours(obj.setHourStartTime,obj.setMinuteStartTime,0);
        const endTime = +new Date().setHours(obj.setHourEndTime,obj.setMinuteEndTime,0);

        obj['startTime'] = startTime;
        obj['endTime'] = endTime;

        return obj;
    }

    return(
        <React.Fragment>
            <div className="container">
                <div className="row">

                    {/*  Batch Data insert  */}
                    <div className="col-sm-6 form-group">
                        <h2> Batch detail update form </h2> <br/>
                        <div className="input-style">
                            <label htmlFor="batchName"> Batch Name </label>
                            <input type="text" name="batchName" onChange={ (e) => handleChange(e) } className="form-control" id="inp-batch-name" placeholder="Batch Name" />
                        </div>
                        <div className="input-style">
                            <div className="row">
                                <div className="col-sm-6">
                                    <label htmlFor="startTime"> Start Time  </label>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <label htmlFor="setHourStartTime"> Set Hour </label>
                                            <input type="number" name="setHourStartTime" onChange={ (e) => handleChange(e) } min="1" max="24" className="form-control" id="inp-batch-start-time-hour" placeholder="24hr format" />
                                        </div>
                                        <div className="col-sm-6">
                                            <label htmlFor="setMinuteStartTime"> Set Minute </label>
                                            <input type="number" name="setMinuteStartTime" onChange={ (e) => handleChange(e) } min="0" max="59" className="form-control" id="inp-batch-start-time-min"  placeholder="eg 59" /> 
                                        </div>
                                    </div>

                                </div>
                                <div className="col-sm-6">
                                    <label htmlFor="endTime"> End Time  </label>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <label htmlFor="setHourEndTime"> Set Hour </label>
                                            <input type="number" name="setHourEndTime" onChange={ (e) => handleChange(e) } min="1" max="24" className="form-control" id="inp-batch-end-time-hour" placeholder="24hr format" />
                                        </div>
                                        <div className="col-sm-6">
                                            <label htmlFor="setMinuteEndTime"> Set Minute </label>
                                            <input type="number" name="setMinuteEndTime" onChange={ (e) => handleChange(e) } min="0" max="59" className="form-control" id="inp-batch-end-time-min"  placeholder="eg 59" /> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="input-style">
                            <label htmlFor="weekDays"> WeekDays </label>
                            <div className="form-check">
                                <div className="row">
                                    <div className="col-sm-3">
                                        <input type="checkbox" name="sunday" onChange={ (e) => handleChange(e) } className="form-check-input" />
                                        <label className="form-check-label" htmlFor="sunday">
                                            Sunday
                                        </label>
                                    </div>
                                    <div className="col-sm-3">
                                        <input name="monday" weekdays="true" onChange={ (e) => handleChange(e) } className="form-check-input" type="checkbox" id="weekday-2" weekday="2" />
                                        <label className="form-check-label" htmlFor="monday">
                                            Monday
                                        </label>
                                    </div>
                                    <div className="col-sm-3">
                                        <input name="tuesday" weekdays="true" onChange={ (e) => handleChange(e) } className="form-check-input" type="checkbox" id="weekday-3" weekday="3" />
                                        <label className="form-check-label" htmlFor="tuesday">
                                            Tuesday
                                        </label>
                                    </div>
                                    <div className="col-sm-3">
                                        <input name="wednesday" weekdays="true" onChange={ (e) => handleChange(e) } className="form-check-input" type="checkbox" id="weekday-4" weekday="4" />
                                        <label className="form-check-label" htmlFor="wednesday">
                                            Wednesday
                                        </label>
                                    </div>
                                    <div className="col-sm-3">
                                        <input name="thursday" weekdays="true" onChange={ (e) => handleChange(e) } className="form-check-input" type="checkbox" id="weekday-5" weekday="5" />
                                        <label className="form-check-label" htmlFor="thrusday">
                                            Thursday
                                        </label> 
                                    </div>
                                    <div className="col-sm-3">
                                        <input name="friday" weekdays="true" onChange={ (e) => handleChange(e) } className="form-check-input" type="checkbox" id="weekday-6" weekday="6" />
                                        <label className="form-check-label" htmlFor="friday">
                                            Friday
                                        </label>
                                    </div>
                                    <div className="col-sm-3">
                                        <input name="saturday" weekdays="true" onChange={ (e) => handleChange(e) } className="form-check-input" type="checkbox" id="weekday-7" weekday="7" />
                                        <label className="form-check-label" htmlFor="saturday">
                                            Saturday
                                        </label>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div className="input-style">
                            <div className="row">
                                <div className="col-sm-6">
                                    <label htmlFor="batchFees"> Batch Fees </label>
                                    <input name="batchFees" onChange={ (e) => handleChange(e) } type="number" min="0" className="form-control" id="inp-batch-fees" placeholder="eg. 1200" />
                                </div>
                                <div className="col-sm-6">
                                    <label htmlFor="batchCapacity"> Batch Capacity </label>
                                    <input name="batchCapacity" onChange={ (e) => handleChange(e) } type="number" min="0" className="form-control" id="inp-batch-capacity" placeholder="eg. 25" />
                                </div>
                            </div>
                        </div>
                        <div className="input-style">
                            <div className="row">
                                <div className="col-sm-12">
                                    <label htmlFor="fitnessDetail"> Fitness Detail </label>
                                    <textarea name="fitnessDetail" onChange={ (e) => handleChange(e) } className="form-control" placeholder="eg. run , excercise , workout.. etc" id="inp-fitness-detail" cols="30" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="input-style">
                            <div className="row">
                                <div className="col-sm-6">
                                    <label htmlFor="batchStatus"> Batch Status </label>
                                    <select name="batchStatus" onChange={ (e) => handleChange(e) } className="custom-select" id="select-batch-status">
                                        <option value="">  </option>
                                        <option value="active"> active </option>
                                        <option value="inactive"> inactive </option>
                                    </select>
                                </div>
                                <div className="col-sm-6">
                                    <br/>
                                    <button type="button" className="btn btn-success" onClick={ ()=>{ formSubmit() } } id="batch-form-submit"> Submit </button>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                   {/* Active Batch Data Display */}
                    <div className="col-sm-6">
                        <h2> Existing Batches </h2>
                        <br/>
                        {
                            (getError)? <span key="err-msg"> oops getting err while fetching... </span>  :
                            batchData.map( d => {
                                return (
                                            <div className="batch-detail square-border-style" id={d.bid} key={'batch_id_'+d.bid}>
                                                <span key={d.bid+'_batch_name'}> Batch Name : {d.batch_name} </span> 
                                                <span key={d.bid+'_star_time'}> Start Time : {formatAMPM(d.start_time)} </span> 
                                                <span key={d.bid+'_end_time'}> End Time : {formatAMPM(d.end_time)} </span> 
                                                <span key={d.bid+'_batch_capcity'}> Batch Capacity : {d.batch_capacity} </span> 
                                                <span key={d.bid+'_batch_fess'}> Batch Fees : {d.batch_fees} </span> 
                                                <span key={d.bid+'_batch_days'}> WeekDays : { d.weekdays } </span> 
                                                <button key={d.bid+'_batch_enquiry'} type="button" className="btn btn-outline-primary btn-sm ">Enquiry</button> 
                                                <button key={d.bid+'_batch_apply'} type="button" className="btn btn-outline-info btn-sm ml-2"> Apply Now </button> 
                                            </div>
                                        )
                            })
                        }
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
export default BatchUpdationComponent;