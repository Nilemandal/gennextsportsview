import { getValueById } from '../../helper/getElementValue';

var exportDataObj;
var flag = false;
window.addEventListener('load' , async ()=>{

    var el = document.getElementsByClassName('form-control');
    if(el[0]){
        el[0].addEventListener('input' , async () =>{
            flag = await validityTest( el[0].value , el[0].id );
        })
    
        el[1].addEventListener('input' , async ()=>{
            flag = await validityTest( el[1].value , el[1].id );
        })
    
        el[2].addEventListener('input' , async ()=>{
            flag = await validityTest( el[2].value , el[2].id );
        })
    
        el[3].addEventListener('input' , async ()=>{
            flag = await validityTest( el[3].value , el[3].id );
        })
    
        el[4].addEventListener('input' , async ()=>{
            flag = await validityTest( el[4].value , el[4].id );
        })
    
        el[5].addEventListener('input' , async ()=>{
            flag = await validityTest( el[5].value , el[5].id );
        })
    
        el[6].addEventListener('input' , async ()=>{
            flag = await validityTest( el[6].value , el[6].id );
        })
    
        el[7].addEventListener('input' , async ()=>{
            flag = await validityTest( el[7].value , el[7].id );
        })
    }

    var batchSubmitBtn = document.getElementById('batch-form-submit');
    if(batchSubmitBtn){ 
        batchSubmitBtn.addEventListener('click' , async ()=>{
            if(flag){
                let weekDays = document.querySelectorAll('.form-check-input:checked');
                let weekDaysObj = [];
                if(weekDays.length > 1){
                    for(let i = 0; i < weekDays.length; i++){
                        weekDaysObj.push( Number(weekDays[i].getAttribute('weekday')));
                    }
                }else alert('weekdays not selected min 2');

                const startTime = +new Date().setHours( await getValueById('inp-batch-start-time-hour'),await getValueById('inp-batch-start-time-min'),0);
                const endTime = +new Date().setHours( await getValueById('inp-batch-end-time-hour'),await getValueById('inp-batch-end-time-min'),0);
                const batchStatus = document.getElementById('select-batch-status').options[document.getElementById('select-batch-status').selectedIndex].text;

                exportDataObj = {
                    batchName: document.getElementById('inp-batch-name').value,
                    startTime,
                    endTime,
                    weekDays: weekDaysObj,
                    batchFees: document.getElementById('inp-batch-fees').value,
                    batchCapacity: document.getElementById('inp-batch-capacity').value,
                    fitnessDetail: document.getElementById('inp-fitness-detail').value,
                    batchStatus: batchStatus,
                }
            }else alert('something is empty do check n submit again');
        })
    }
    
});

async function validityTest(value , id){
    return new Promise( (resolve,rejects) =>{
        if(value.length > 1){
            document.getElementById(id).classList.remove('is-invalid');
            document.getElementById(id).classList.add('is-valid');
            resolve(true); // flag boolean
        }else{
            document.getElementById(id).classList.remove('is-valid');
            document.getElementById(id).classList.add('is-invalid');
            resolve(false); // flag boolean
        }
    })
}

export { 
    exportDataObj,
    flag
};