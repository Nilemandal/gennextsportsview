import React , { useState ,useEffect } from 'react'
import { Container , Row , Col , Card , ListGroup , Button , Modal , Form } from "react-bootstrap";
import NotificationManager from 'react-notifications/lib/NotificationManager';
import postRequest from "../../api/postRequest";
import { monthYear , monthNames } from "../../helper/dateFunction";

function Performance() {
    //const [ admin , setAdmin ] = useState({});
    const [ activeUser, setActiveUser ] = useState([])
    const [ perModal , setPerModal ] = useState(false)
    const [ modalUser , setModalUser ] = useState({})

    const getActiveUser = async (obj) => {
        const url = '/api/get-active-cricket-user';
        let getActiveUser = await postRequest(url,obj);
        if(getActiveUser.status === 'success'){
            let activeUser = getActiveUser.data;
            if(activeUser[0] && activeUser[0].batchProcessId ){
                setActiveUser(activeUser);
            }
        }else{
            setActiveUser([]);
        }
        
    }

    useEffect(() => {
        let month = new Date().getUTCMonth();
        let year = new Date().getUTCFullYear();

        let maxDaysMonth = [0,2,4,6,7,9,11];
        let days = maxDaysMonth.includes(month)?31:30;
        days = (month === 1)?28:days;

        let startTime = +new Date(year,month,1);
        let endTime = +new Date(year,month,days);

        getActiveUser({
            start: startTime,
            end: endTime
        });

    }, [])

    async function handleClick(e){
        let month = e.target.attributes.getNamedItem('month').value;
        let year = e.target.attributes.getNamedItem('year').value;
        let monNum = e.target.attributes.getNamedItem('num').value - 1;

        let maxDaysMonth = [0,2,4,6,7,9,11];
        let days = maxDaysMonth.includes(monNum)?31:30;
        days = (month === 1)?28:days;

        let startTime = +new Date(year,monNum,1);
        let endTime = +new Date(year,monNum,days);
        
        const obj = {
            start : startTime,
            end : endTime
        }
        getActiveUser(obj);
    }

    const toggle = () => {
        setPerModal(false);
    }

    let modalData = '';
    async function openPerformanceModal(d){
        setModalUser(d)
        setPerModal(true);
    }
    if(perModal){
        modalData = <PerformanceModal toggle={toggle} user={modalUser} modal={perModal} />
    }

    return ( 
        <div>
            <Container fluid>
                <Row>
                    <Col xs={11} sm={4} md={3}>
                    <Card border="dark" style={{  marginTop : '1rem', overflowY: 'scroll', height: 'calc(100vh - 127px)'  }}>
                        <Card.Header> Month Year </Card.Header>
                        <ListGroup variant="flush">
                            <ListGroup.Item onClick={(e)=> handleClick(e) } month={'Jun'} num={5} year={'2021'} > June 2021 </ListGroup.Item>
                            <ListGroup.Item onClick={(e)=> handleClick(e) } month={'May'} num={4} year={'2021'} > May 2021 </ListGroup.Item>
                        </ListGroup>
                    </Card>
                    </Col>
                    <Col xs={11} sm={8} md={9}>
                        <Card border="dark" style={{ width: "100%" ,  overflowY: 'scroll', marginTop : '1rem', height: 'calc(100vh - 127px)'}}>
                            <Card.Header> Active Users </Card.Header>
                            <Card.Body>
                                <ListGroup variant="flush">
                                    {   
                                        ( activeUser[0] && activeUser[0].batchProcessId )?
                                        activeUser.map( (user , index)=>{
                                            return(
                                                <ListGroup.Item key={"activeCricketers_"+user.batchProcessId}>
                                                    <div>
                                                        { index + 1 }. { user.batchName } | {  user.batchStartDate } | { user.batchFees } | { user.name } | { user.phone }  &nbsp;
                                                        <Button variant="info" size="sm" onClick={ ()=> { openPerformanceModal(user) } } > Performance </Button>  &nbsp;
                                                        {/* <Button variant="warning" size="sm"> Attendance </Button> */}
                                                    </div>
                                                </ListGroup.Item>
                                            )
                                        })
                                        :
                                        'No Active User'
                                    }
                                </ListGroup>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                {   
                    modalData
                }
            </Container>
        </div>
    )
}

const PerformanceModal = ({modal,user,toggle}) => {
    let keyName = monthNames[new Date().getMonth()]+'_'+new Date().getFullYear();
    const oldPerformance = (JSON.parse(user.performance)[keyName])?JSON.parse(user.performance)[keyName]:{};
    const [ performanceData , setPerformanceData ] = useState(oldPerformance);

    const setPerformance = (el) => {
        let elName = el.target.name;
        let value = el.target.value;

        performanceData[elName] = value;

        setPerformanceData({
            ...performanceData
        })
    }

    const submitPerformance = async () => {
        let obj = {
            performance:JSON.stringify(performanceData),
            batchProcessId:user.batchProcessId,
            month:+new Date().getMonth()
        }
        const url = '/api/update-batch-performance'
        let perRes = await postRequest(url,obj);
        if(perRes.status === 'success'){
            NotificationManager.info('Performance updated successfully','Info',2000);
            toggle();
        }else{
            NotificationManager.error(perRes.reason,'Error',2000);
            toggle();
        }
    }

    return(
        <Modal show={modal} onHide={toggle}>
            <Modal.Header closeButton>
                <Modal.Title> Update Performance | { monthYear(new Date()) } </Modal.Title>
            </Modal.Header>
            <Modal.Body> 
                <span> 
                    USER DETAIL : { user.name } | { user.phone } | {user.batchName} 
                </span>
                <hr />
                <Form>
                    <Form.Group controlId="battig">
                        <Form.Label> Batting </Form.Label>
                        &nbsp; <span> [ {performanceData.batting} ] % </span>
                        <Form.Control type="range" min="1" max="100" name="batting" defaultValue={performanceData.batting || 0} onChange={ (e)=>{ setPerformance(e) } } />
                    </Form.Group>
                    <Form.Group controlId="bowling">
                        <Form.Label> Bowling </Form.Label>
                        &nbsp; <span> [ { performanceData.bowling } ] % </span>
                        <Form.Control type="range" min="1" max="100" name="bowling" defaultValue={performanceData.bowling || 0} onChange={ (e)=>{ setPerformance(e) } } />
                    </Form.Group>
                    <Form.Group controlId="fielding">
                        <Form.Label> Fielding </Form.Label>
                        &nbsp; <span> [ { performanceData.fielding } ] % </span>
                        <Form.Control type="range" min="1" max="100" name="fielding" defaultValue={performanceData.fielding || 0} onChange={ (e)=>{ setPerformance(e) } } />
                    </Form.Group>
                    <Form.Group controlId="attendance">
                        <Form.Label> Attendance </Form.Label>
                        &nbsp; <span> [ {performanceData.attendance} ] % </span> 
                        <Form.Control type="range" min="1" max="100" name="attendance" defaultValue={performanceData.attendance || 0} onChange={ (e)=>{ setPerformance(e) } } />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={toggle}>
                    Close
                </Button>
                <Button variant="primary" onClick={submitPerformance}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default Performance