import React from "react";
import { Button } from "react-bootstrap";

const AdimNavigation = (props) => {

    const handleChange = (e) => {
        props.handleChange(e.target.name); 
    }

    return(
        <React.Fragment>
            <div className="mb-3" style={{height:'auto',backgroundColor:"teal",color:"white"}}>
                <Button className="btn btn-light mt-2 ml-2 mb-2" name="batchStatus" onClick={ (e)=> { handleChange(e) } } >
                    Cricket Batch
                </Button>
                <Button className="btn btn-light mt-2 ml-2 mb-2" name="batchPerformance" onClick={ (e)=> { handleChange(e) } }>
                    Performance
                </Button>
                <Button className="btn btn-light mt-2 ml-2 mb-2" name="cricketTourStatus" onClick={ (e)=> { handleChange(e) } }>
                    Cricket Tour
                </Button>
            </div>
        </React.Fragment>   
    )
}

export default AdimNavigation;