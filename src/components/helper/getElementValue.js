export async function getValueById(id){
    return document.getElementById(id).value;
}

export async function getValueByClass(className){
    return document.getElementsByClassName(className).value;
}

export async function getAttributeValue(key , keyName , attributeName){
    if(key === 'id')  return document.getElementById(keyName).getAttribute(attributeName);
    else return document.getElementsByClassName(keyName).getAttribute(attributeName);
}
