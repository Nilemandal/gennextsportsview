export function dateTime(timestamp){
    let d = (new Date(timestamp)).getDate();
    let m = (new Date(timestamp)).getMonth();
    let y = (new Date(timestamp)).getFullYear();

    // let hours = (new Date(timestamp)).getUTCHours();
    // let min = (new Date(timestamp)).getMinutes();
    return d+'/'+(m+1)+'/'+y;
}

export function formatAMPM(date) {
    date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

const monthNames = [
                        "January", 
                        "February", 
                        "March", 
                        "April", 
                        "May", 
                        "June",
                        "July", 
                        "August", 
                        "September",
                        "October",
                        "November", 
                        "December"
                    ];

export { monthNames };

export function monthYear(timestamp){
    let month = monthNames[ new Date(timestamp).getMonth() ];
    let year = new Date(timestamp).getFullYear();
    return month +' '+ year;
}

export function countDays(start , end){
    return( Math.round((end - start) / (1000 * 60 * 60 * 24) ));
}

export function weekDaysFunc(weakDays){
    let weaks = {
        1:'Mon',
        2:'Tue',
        3:'Wed',
        4:'Thu',
        5:'Fri',
        6:'Sat',
        7:'Sun'
    }
    weakDays = weakDays.split(',');
    return weakDays.map( (days) => {
        return weaks[days]+" "
    })
}