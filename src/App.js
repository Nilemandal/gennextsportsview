import React from 'react';
import './App.css';
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Home from "./components/pages/home";
import AboutUs from "./components/pages/about";
import Contact from "./components/pages/contact";
import Sports from "./components/pages/sports";
import Clothes from "./components/pages/clothes";
import Index from "./components/pages/admin/index";
import Dashboard from "./components/pages/dashboard";
import Header from './components/pages/layout.js/head';
import Performance from "./components/admin/performance/Performance";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from './components/pages/layout.js/footer/footer';
import 'react-notifications/lib/notifications.css';
import NotificationContainer from 'react-notifications/lib/NotificationContainer';

function App() {
  return (
    <div className="page-container">
      <div className="content-wrap">
      <NotificationContainer/>
        <Router>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={AboutUs} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/sports" component={Sports} />
            <Route exact path="/clothes" component={Clothes} />
            <Route exact path="/admin" component={Index} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/admin/performance" component={Performance} />
          </Switch>
          <Footer />
        </Router>
      </div>
    </div>
  );
}

export default App;
